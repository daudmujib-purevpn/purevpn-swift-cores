//
//  EventTracker.swift
//  PureVPN
//
//  Created by Daud Mujib on 08/04/2020.
//

import Mixpanel

public struct EventTracker {
    
    public func start() {
        Mixpanel.initialize(token: "")
        Mixpanel.mainInstance().track(event: "Test Event")
    }
}

import XCTest

import PureVPNTests

var tests = [XCTestCaseEntry]()
tests += PureVPNTests.allTests()
XCTMain(tests)

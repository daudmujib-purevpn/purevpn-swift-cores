import XCTest
@testable import PureVPN

final class PureVPNTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(PureVPN().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
